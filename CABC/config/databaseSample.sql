
CREATE TABLE COMPANYIA (
	id INTEGER PRIMARY KEY,
	nom VARCHAR2(70) NOT NULL
);

CREATE TABLE AVIO (
	id INTEGER PRIMARY KEY,
	nom VARCHAR2(50) NOT NULL,
	model VARCHAR2(50),
	dataFabricacio DATE,
	nSeients INTEGER,
  	idCompanyia INTEGER NOT NULL,	
  	CONSTRAINT fk_idCompanyia FOREIGN KEY(idCompanyia) REFERENCES COMPANYIA(id)
);

CREATE TABLE CLIENT (
	nif VARCHAR2(9) PRIMARY KEY,
	nom VARCHAR2(100) NOT NULL,
	telefon1 VARCHAR2(12),
	telefon2 VARCHAR2(12)
);

CREATE TABLE EMPRESA (
	nifClient VARCHAR2(9) PRIMARY KEY,
	dtePerTreballador BINARY_FLOAT,
	quantTreballadors INTEGER,
	CONSTRAINT fk_nifClient FOREIGN KEY(nifClient) REFERENCES CLIENT(nif)
);

CREATE TABLE CLIENTVIP (
	nifClient VARCHAR2(9) PRIMARY KEY,
	alergies NUMBER(1) DEFAULT 0,
	    CONSTRAINT cK_ALERGIES CHECK( alergies IN (0,1) ),
	quantTreballadors INTEGER,
	CONSTRAINT fk_nifClientvip FOREIGN KEY(nifClient) REFERENCES CLIENT(nif)
);




CREATE TABLE AEROPORT (
	id INTEGER PRIMARY KEY,
	nom VARCHAR2(50) NOT NULL,
	poblacio VARCHAR2(50) 
);

CREATE TABLE VOL (
	id VARCHAR2(20) PRIMARY KEY,
	dataHoraSortida TIMESTAMP,
	dataHoraArribada TIMESTAMP,
	preu BINARY_FLOAT,
	seientsLliures INTEGER,
	idAeroportOrigen INTEGER,
	  	CONSTRAINT fk_AeroportOrigen FOREIGN KEY(idAeroportOrigen) REFERENCES AEROPORT(id),
	idAeroportDesti INTEGER,
	  	CONSTRAINT fk_AeroportDesti FOREIGN KEY(idAeroportDesti) REFERENCES AEROPORT(id)
);

CREATE TABLE SEIENT (
	num INTEGER,
	estat VARCHAR2(6) DEFAULT 'LLIURE',
    CONSTRAINT cK_ESTAT CHECK( estat IN ('LLIURE','OCUPAT') ),
	idAvio INTEGER,
	idVol VARCHAR2(20),
	CONSTRAINT pk_Seient PRIMARY KEY(idAvio, idVol, num),
	CONSTRAINT fk_idAvio FOREIGN KEY(idAvio) REFERENCES AVIO(id),
	CONSTRAINT fk_idVol FOREIGN KEY(idVol) REFERENCES VOL(id)
);


CREATE TABLE BITLLET (
	id VARCHAR2(50) PRIMARY KEY,
	seientAssignat INTEGER,
	finger VARCHAR2(20),
	nifClient VARCHAR2(9),
	idVol VARCHAR2(20),
	CONSTRAINT fk_Client FOREIGN KEY(nifClient) REFERENCES CLIENT(nif),
	CONSTRAINT fk_Vol FOREIGN KEY(idVol) REFERENCES VOL(id)
);

CREATE TABLE LLISTAESPERA (
	nifClient VARCHAR2(9),
	idVol VARCHAR2(20),
	CONSTRAINT pk_llistaEspera PRIMARY KEY(nifClient,idVol),
	CONSTRAINT fk_VolLlistaEspera FOREIGN KEY(idVol) REFERENCES VOL(id),
	CONSTRAINT fk_ClientLlistaEspera FOREIGN KEY(nifClient) REFERENCES CLIENT(nif)
);

REM INSERTING into COMPANYIA
Insert into COMPANYIA(id,nom) values ('1','Vueling');
Insert into COMPANYIA(id,nom) values ('2','Turkish Airlines');
Insert into COMPANYIA(id,nom) values ('3','Iberia');
Insert into COMPANYIA(id,nom) values ('4','Swiftair');
Insert into COMPANYIA(id,nom) values ('5','Airnor');
commit;