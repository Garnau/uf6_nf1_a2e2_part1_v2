package net.infobosccoma.cabc.model;

/**
 * Classe que modela la informació de Companyies
 *
 * @author usuari
 */ 
public class Companyies {

//<editor-fold defaultstate="collapsed" desc="Atributs d'instància">
    private int id;
    private String nom;
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Constructors">
    public Companyies() {
        this(0, "");
    }

    public Companyies(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Getters/Setters">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="Mètodes d'instància">
    @Override
    public String toString() {
        return String.format("ID: %d\nNOM: %s\n------------------\n",
                this.id, this.nom);
    }
//</editor-fold>

}
