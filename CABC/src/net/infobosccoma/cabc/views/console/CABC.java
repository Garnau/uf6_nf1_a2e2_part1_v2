package net.infobosccoma.cabc.views.console;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.infobosccoma.cabc.model.Companyies;

public class CABC {

    static final Logger LOGGER = Logger.getLogger(CABC.class.getName());

    static final String ORACLE_CONFIG_FILE = "config/oracle.properties";

    static final String ID_COMPANYIA_COLUMN_NAME = "id";
    static final String NOM_COMPANYIA_COLUMN_NAME = "nom";

    static final String MOSTRAR_COMPANYIES = "SELECT * FROM COMPANYIA";

    static final String MOSTRAR_COMPANYIA_BY_NOM = String.format("SELECT %s, %s FROM COMPANYIA WHERE nom = ?",
            ID_COMPANYIA_COLUMN_NAME,
            NOM_COMPANYIA_COLUMN_NAME);

    static final String COMPANYIES_QUERY_STR = String.format("SELECT %s, %s FROM COMPANYIA",
            ID_COMPANYIA_COLUMN_NAME,
            NOM_COMPANYIA_COLUMN_NAME);

    static final String EDITAR_NOM_COMPANYIA = String.format("UPDATE COMPANYIA SET nom = ? WHERE id = ?");

    static final String AFEGIR_COMPANYIA = String.format("INSERT INTO COMPANYIA (id,nom) VALUES(?,?)");
    
    static final String ESBORRAR_COMPANYIA = String.format("DELETE FROM COMPANYIA WHERE id = ?");

    static final Scanner reader = new Scanner(System.in);

    static Integer select = 0;

    /**
     * Programa principal
     *
     * @param args
     */
    public static void main(String[] args) {
        // obrir la connexió
        System.out.println("Connectant amb la BD... \n");
        try (Connection connection = openConnection()) {

            FileHandler fd = new FileHandler("./exemplejdbc.log");
            fd.setLevel(Level.ALL);
            LOGGER.addHandler(fd);

            System.out.println("Companyia Aèria de Baix Cost");
            System.out.println("=============================== \n");

            System.out.println("1 - Mostrar totes les companyies");
            System.out.println("2 - Mostrar una companyia");
            System.out.println("3 - Afegir nova companyia");
            System.out.println("4 - Modificar companyia");
            System.out.println("5 - Esborrar companyia");
            System.out.println("6 - Sortir");

            do {
                System.out.print("\nEscull una opció: ");
                select = reader.nextInt();
                System.out.println("");

                switch (select) {
                    case 1:

                        //Mostrem companyies
                        mostrarCompanyies(connection);

                        System.out.println("---------------------");
                        break;

                    case 2:

                        //Mostrar companyia seleccionada
                        System.out.print("Quina companyia vols mostrar? ");
                        String nomCompanyia = reader.next();
                        System.out.println("");

                        //Cridem a la funció mostrarCompanyia
                        mostrarCompanyia(connection, nomCompanyia);

                        System.out.println("---------------------");
                        break;

                    case 3:

                        //Afegir companyia nova
                        System.out.println("AFEGIR COMPANYIA\n----------------\n");
                        System.out.print("Nom: ");
                        nomCompanyia = reader.next();

                        //Cridem a la funció afegirCompanyia
                        if (afegirCompanyia(connection, nomCompanyia) > 0) {
                            System.out.println("\nCOMPANYIA AFEGIDA CORRECTAMENT\n");
                        } else {
                            System.out.println("\nINSERT FALLIT\n");
                        }

                        System.out.println("---------------------");
                        break;

                    case 4:

                        //Modificar una companyia
                        System.out.println("MODIFICAR COMPANYIA\n------------------\n");
                        System.out.print("Id: ");
                        Integer idCompanyia = reader.nextInt();

                        System.out.print("Nou nom: ");
                        nomCompanyia = reader.next();

                        if (modificarCompanyia(connection, idCompanyia, nomCompanyia) > 0) {
                            System.out.printf("\nUPDATE AFECTUAT CORRECTAMENT\n");
                        } else {
                            System.out.println("\nUPDATE FALLIT\n");
                        }

                        System.out.println("\n---------------------");
                        break;

                    case 5:
                        
                        //Esborrem una companyia
                        System.out.println("ESBORRAR COMPANYIA\n----------------\n");

                        System.out.print("Id: ");
                        idCompanyia = reader.nextInt();

                        //Cridem a la funció afegirCompanyia
                        if (esborrarCompanyia(connection, idCompanyia) > 0) {
                            System.out.printf("\nCOMPANYIA ESBORRADA CORRECTAMENT\n");
                        } else {
                            System.out.println("\nDELETE FALLIT\n");
                        }

                        System.out.println("\n---------------------");
                        break;

                    case 6:
                        System.out.println("FINS AVIAT!");
                        break;
                }
            } while (select != 6);

        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

//<editor-fold defaultstate="collapsed" desc="openConnection">
    /**
     * Obre la connexió amb la base de dades
     *
     * @return la connexió oberta
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    static Connection openConnection() throws IOException, ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        try (FileInputStream in = new FileInputStream(ORACLE_CONFIG_FILE)) {
            Properties configOracle = new Properties();
            configOracle.load(in);

            String url = configOracle.getProperty("dburl");
            String driver = configOracle.getProperty("dbdriver");
            String userName = configOracle.getProperty("dbuser");
            String password = configOracle.getProperty("dbpassword");

            Class.forName(driver).newInstance();
            return (Connection) DriverManager.getConnection(url, userName, password);
        }
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="mostrarCompanyies">
    /**
     * Mostrar totes les companyies
     *
     * @param connection la connexió a la base de dades
     * @throws SQLException
     */
    static void mostrarCompanyies(Connection connection) throws SQLException {

        PreparedStatement query = connection.prepareStatement(MOSTRAR_COMPANYIES);

        System.out.println("LLISTAT DE COMPANYIES: " + "\n---------------------\n");

        showCOMPANYIES(query.executeQuery());
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="mostrarCompanyia">
    /**
     * Mostrar una companyia per nom
     *
     * @param connection la connexió a la base de dades
     * @throws SQLException
     */
    static void mostrarCompanyia(Connection connection, String nomCompanyia) throws SQLException {
        PreparedStatement query = connection.prepareStatement(MOSTRAR_COMPANYIA_BY_NOM);

        // Consultem la persona amb nom = aux
        query.setString(1, nomCompanyia);

        showCOMPANYIES(query.executeQuery());

    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="afegirCompanyies">
    /**
     * Modifica nom d'una companyia
     *
     * @param connection la connexió a la base de dades
     * @throws SQLException
     */
    static int afegirCompanyia(Connection connection, String nomCompanyia) throws SQLException {

        int res = 0;
        PreparedStatement queryID = connection.prepareStatement("SELECT MAX(id) as maxId FROM COMPANYIA");

        ResultSet rs = queryID.executeQuery();

        if (rs.next()) {

            PreparedStatement query = connection.prepareStatement(AFEGIR_COMPANYIA);

            //Agafem l'últim id de la query i l'incrementem
            int id = rs.getInt("maxId") + 1;

            //Assignem el camp id i nom a la query
            query.setInt(1, id);
            query.setString(2, nomCompanyia);

            //query.executeQuery == SELECTS \\ query.executeUpdate == UPDATES...
            //PER DEFECTE LA QUERY ESTÀ EN AUTO COMMIT
            res = query.executeUpdate();

        }

        return res;

    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="modificarCompanyia">
    /**
     * Modifica nom d'una companyia
     *
     * @param connection la connexió a la base de dades
     * @throws SQLException
     */
    static int modificarCompanyia(Connection connection, Integer idCompanyia, String nomCompanyia) throws SQLException {
        PreparedStatement query = connection.prepareStatement(EDITAR_NOM_COMPANYIA);

        //Assignem el camp id i nom a la query
        //1 = valor a canviar 
        //2 = where, la condició
        query.setString(1, nomCompanyia);
        query.setInt(2, idCompanyia);
        
        //PER DEFECTE LA QUERY ESTÀ EN AUTO COMMIT
        int res = query.executeUpdate();

        return res;

    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="esborrarCompanyia">
    /**
     * Esborrar una companyia
     *
     * @param connection la connexió a la base de dades
     * @throws SQLException
     */
    static int esborrarCompanyia(Connection connection, Integer idCompanyia) throws SQLException {

        int res = 0;

        PreparedStatement query = connection.prepareStatement(ESBORRAR_COMPANYIA);
        
        //Assignem nom a la query
        query.setInt(1, idCompanyia);

        //PER DEFECTE LA QUERY ESTÀ EN AUTO COMMIT
        res = query.executeUpdate();

        return res;

    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="showCOMPANYIES">
    /**
     * Per cada registre de la consulta trobat, cridarà a showCompanyia per
     * mostrar-la.
     *
     * @param result l'objecte RestultSet amb els registres a mostrar
     * @throws SQLException
     */
    static void showCOMPANYIES(ResultSet result) throws SQLException {
        while (result.next()) {
            showCompanyia(result);
        }
        result.close();
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="showCompanyia">
    /**
     * Mostra un registre de la taula "COMPANYIES"
     *
     * @param result Conté el conjunt de resultats, apuntant a una persona
     * @throws SQLException
     */
    private static void showCompanyia(ResultSet result) throws SQLException {
        System.out.printf("ID: %d\n", result.getInt(ID_COMPANYIA_COLUMN_NAME));
        System.out.printf("NOM: %s\n", result.getString(NOM_COMPANYIA_COLUMN_NAME));
        System.out.println("");
    }
//</editor-fold>

}
