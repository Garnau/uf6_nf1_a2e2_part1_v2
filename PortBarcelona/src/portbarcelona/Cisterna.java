/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portbarcelona;

/**
 *
 * @author usuari
 */
public class Cisterna extends Contenidor {
    
    private double capacitat = 0;

    public Cisterna(String nSerie, double capacitat, int qMercaderies, String estat, String tipus) {
            super(nSerie, capacitat, qMercaderies, estat, "Cisterna");
            
            setCapacitat(capacitat);
    }
      
    //<editor-fold defaultstate="collapsed" desc="GETTER SETTER">
    @Override
    public double getCapacitat() {
        return capacitat;
    } 
    
    public void setCapacitat(double capacitat) {
        this.capacitat = 1000 * capacitat;
    }
    //</editor-fold>

}
