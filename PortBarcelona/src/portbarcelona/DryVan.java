/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portbarcelona;

public class DryVan extends Contenidor {

    private String color;
    
    public DryVan(String nSerie, double capacitat, int qMercaderies, String estat, String color, String tipus) {
        super(nSerie, capacitat, qMercaderies, estat, tipus);

        this.color = color;
    }

    //<editor-fold defaultstate="collapsed" desc="GETTER">
    public String getColor() {
        return color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    //</editor-fold>
    
}
