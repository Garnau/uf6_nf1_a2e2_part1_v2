/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portbarcelona;

public class Contenidor {

    public static final int MAX_CAPA = 100;

    private String nSerie = "";
    private double capacitat = 0;
    private Mercaderia[] mercaderies = new Mercaderia[MAX_CAPA];
    private int qMercaderies = 0;
    private String estat = "";
    private String tipus = "";
    private String color = "";
    private double temp = 0;

    public Contenidor(String nSerie, double capacitat, int qMercaderies, String estat, String tipus) {
        this.nSerie = nSerie;
        this.capacitat = capacitat;
        this.qMercaderies = qMercaderies;
        this.estat = "obert";
        this.tipus = tipus;
    }

    public void afegirMercaderies(Mercaderia mercaderia) {

        if (this.getTipus().equals("Cisterna")) {
            this.mercaderies[0] = mercaderia;
            this.setEstat("tancat");
        } else if (this.qMercaderies + 1 < 100) {

            if (mercaderies[qMercaderies + 1] == null) {
                this.mercaderies[qMercaderies + 1] = mercaderia;
                qMercaderies++;

            }

            if (this.qMercaderies == 100) {
                this.setEstat("tancat");
            }

        } else {
            System.out.println("S'han superat el màxim de mercaderies.");
        }
    }

    //<editor-fold defaultstate="collapsed" desc="GETTER SETTER">

    public double getTemp() {
        return temp;
    }    
    
    public String getColor() {
        return color;
    }  
    
    public String getnSerie() {
        return nSerie;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public double getCapacitat() {
        return capacitat;
    }

    public Mercaderia[] getMercaderies() {
        return mercaderies;
    }

    public int getqMercaderies() {
        return qMercaderies;
    }

    public String getEstat() {
        return estat;
    }

    public void setnSerie(String nSerie) {
        this.nSerie = nSerie;
    }

    public void setCapacitat(double capacitat) {
        this.capacitat = capacitat;
    }

    public void setMercaderies(Mercaderia[] mercaderies) {
        this.mercaderies = mercaderies;
    }

    public void setqMercaderies(int qMercaderies) {
        this.qMercaderies = qMercaderies;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    //</editor-fold>       
}
